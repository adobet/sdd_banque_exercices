Mot de Fibonacci
+++++++++++++++++++++++++++++++++

Le mot de Fibonacci est une suite infinie de `0` et de `1` qui a de nombreuses propriétés intéressantes en mathématiques et physique théorique.

Il est lié notamment aux quasi-cristaux, dont voici un exemple ci-dessous

  .. image :: qc-diffraction.jpg

Il est aussi lié aux *pavages apériodiques*, dont on trouve des exemples dans l'art islamique, en particulier persan.

  .. image :: ispahan.jpg
  

La *substitution de fibonacci* est définie sur une liste de `0` et de `1`. À partir de cette liste, on crée une nouvelle liste ainsi: pour chaque `0` dans la liste originale, on ajoute un `1` dans la nouvelle liste; pour chaque `1`, on ajoute un `1` et un `0`.

Voici quelques exemples:

.. csv-table:: Tableau1
   :header: "liste", "résultat de la substitution"
   :widths: 40, 100

   "[0]", "[1] "
   "[1]", "[1, 0]"
   "[1, 0]", "[1, 0, 1]"
   "[0, 0]", "[1, 1]"
   "[0, 1, 0]", "[1, 1, 0, 1]"
   

#. Écrire une fonction `substFibo(liste)` qui effectue la transformation ci-dessus sur une liste de `0` et de `1`.

#. Écrire une fonction `substFiboRepetee(n)` qui à partir d'un entier `n`, renvoie la liste obtenue en transformant `n` fois la liste `[1]`.

Par exemple,

.. code-block:: python 
   
   >>> substFiboRepetee(0)
   [1]
   >>> substFiboRepetee(1)
   [1,0]
   >>> substFiboRepetee(2)
   [1,0,1]
   >>> substFiboRepetee(3)
   [1,0,1,1,0]

#. Afficher les 10 premières valeurs de ``substFiboRepetee()``, avec chaque liste sur une ligne. Que remarquez-vous?

Il existe une liste *infinie* de `0` et de `1`, qui commence par `10110101101101 011010110110101101 ...` dont la première lettre correspond à `substFiboRepetee(0)`, les deux premières à `substFiboRepetee(1)`, les trois premières à `substFiboRepetee(2)`, les cinq premières à `substFiboRepetee(3)` ...

Ce mot, le *mot de Fibonacci*, a de nombreuses propriétés intéressantes. 

#. Écrire une fonction `lettreMotFibo(n)` qui renvoie la `n`-ième lettre du mot de Fibonacci. Pour cela, on doit trouver un $i$ tel que `substFiboRepetee(i)` ait au moins `n` lettre, et prendre la `n`-ème lettre de `substFiboRepetee(i)`.

Par exemple, `lettreMotFibo(0)` vaut `1`, tandis que `lettreMotFibo(4)` vaut `0`.

#. Écrire une fonction `nLettresMotFibo(n)` qui renvoie les `n` premières lettres du mot de Fibonacci.

Une de ses propriétes, est de se répèter presque, *mais jamais tout à fait*, tous comme les motifs des figures \ref{fig:penrose} et \ref{fig:esfahan}.
