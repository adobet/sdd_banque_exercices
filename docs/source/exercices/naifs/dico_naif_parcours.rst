Boucles sur les dictionnaires
------------------------------

On considère le dictionnaire suivant:

.. code-block:: python 

   dico = {'A': True, 'B': False, 'C': False, 'D': False, 'E': True}  


On peut utiliser parcourir les éléments de `dico` avec la boucle suivante:

.. code-block:: python 

   for (lettre, estVoyelle) in dico.items():
       if estVoyelle:
           print(lettre, 'est une voyelle')
       else:
           print(lettre, 'est une consonne')


Écrire une fonction `soldes(objets_prix, rabais)` qui prend en paramètres :

* un dictionnaire `objets_prix` dont les clés sont des noms d'objets et les valeurs sont leur prix (comme à l'exercice `Modifications de dictionnaires`) ;

* n nombre positif `rabais`


Cette fonction renvoie un nouveau dictionnaire avec les mêmes objets, mais des prix réduits de `rabais\%`.

