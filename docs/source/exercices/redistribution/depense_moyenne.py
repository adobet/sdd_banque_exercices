from hypothesis import given,example 
import hypothesis.strategies as st
from fail_test import fail_test

attendu_etudiant = [ 'depense_moyenne' ]

def close_enough(f1, f2, tolerance):
   if f1 == f2:
      return True # pour 0 et +/- infini
   a1 = abs(f1)
   a2 = abs(f2)
   return (abs(f1 - f2) / max(a1, a2)) < tolerance

@given(st.dictionaries(st.text(min_size=5),
                       st.lists(st.floats(allow_nan=False, allow_infinity=False))))
@example({ 'Dominique': [15, 12, 8, 3],
           'Claude' : [20, 34],
	   'Mérédith' : [52],
	   'Alex' : [8]})
def test_moyenne_ok(code_etu, s):
   if len(s):
      mon_total = sum(sum(d) for d in s.values()) / len(s)
      son_total = code_etu.depense_moyenne(s)
      if not close_enough(mon_total, son_total, 1e-5):
         fail_test("Sur l'entrée %s, vous retournez %s au lieu de %s" % (s, son_total, mon_total))
