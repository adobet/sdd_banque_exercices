Redistribution des richesses
----------------------------

On s'intéresse à un groupe d'amis qui sont partis en vacances ensemble. Chacun a fait des achats
pour le groupe, et on veut équilibrer la participation de chacun : ceux qui ont dépensé le moins
vont verser un peu d'argent à ceux qui ont dépensé plus. Ainsi, chacun aura dépensé la même
somme pour les achats communs au groupe

Indiquer (sur feuille) combien chaque peronne devra verser ou recevoir à la fin des vacances dans l'exemple suivant.

+------------+------------+-----------+-----------+
| Dominique  | Claude     | Mérédith  |Alex       |
+============+============+===========+===========+
| 15         | 20         | 52        | 8         |
+------------+------------+-----------+-----------+
| 12         | 34         |           |           |
+------------+------------+-----------+-----------+
| 8          |            |           |           |
+------------+------------+-----------+-----------+
| 3          |            |           |           |
+------------+------------+-----------+-----------+

Pour les vacances de Mai, on veut une solution algorithmique à ce problème.

On représente les dépenses d'une personne par une liste de ses dépenses. Par exemple, pour représenter les dépenses de Dominique, on pose `depenses_dominique = [15,12,8,3]`

Écrire une fonction `total(depenses_personne)` qui permet de calculer combien une
personne a dépensé au total à partir de sa liste de dépenses.

.. easypython:: total_depenses.py
   :language: pythonHypothesis

On veut maintenant représenter les dépenses de tout le groupe. On utilise pour cela un dictionnaire dont les clés sont les noms des personnes, et la valeur associée est la liste des dépenses de cette personne.

Par exemple, pour le groupe ci-dessus, on obtient le dictionnaire suivant:

```
depenses_mai = { 'Dominique': [15, 12, 8, 3],
                 'Claude' : [20, 34],
		 'Mérédith' : [52],
		 'Alex' : [8]
	       }
```

Écrire une fonction `total_groupe(depenses_groupe)` qui indique combien d'argent a été dépensé au total par le groupe.

.. easypython:: total_groupe.py
   :language: pythonHypothesis

On veut écrire une fonction `depense_moyenne(depenses_groupe)` qui indique combien en moyenne chaque personne du group a dépensé.

.. easypython:: depense_moyenne.py
   :language: pythonHypothesis

Écrire une fonction `avoir(depenses_groupe, personne)` qui indique combien la personne indiquée doit recevoir du reste du groupe (ce sera donc un nombre positif si cette personne doit recevoir de l'argent des autres, et un nombre négatif si elle a moins dépensé et qu'elle doit donc verser de l'argent aux autres).

.. easypython:: avoir_personne.py
   :language: pythonHypothesis
